<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'indianmobilespec');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{$?hQ##)Lv0A^A`X(5=evw-|Pp>K5@J-UtS7%L)2 <n^c;K&Czh;>-d3)NLN~o,d');
define('SECURE_AUTH_KEY',  'cv@#dhNL0X?LHvd(=ZiaZcJ~ FBytmEeeH[gX(Hb0lw?*!5b4`s/7.<!+oO}ME+=');
define('LOGGED_IN_KEY',    'np( YYd.oDR=1HZ$Z2n9QazwDbK^,||fS1TR$DgJ%9P=#*)QE)>ShdpWz7$E[*V<');
define('NONCE_KEY',        '5&qy>u[#?edm-YfU8|k/O;Lp5:l:.<ZxB^oA3=x$KsRt>nSM)cKRfw-9`Q.fm!-v');
define('AUTH_SALT',        '^*M{`=o?rgATU6BT=0X$/ncXl |dW;dBL%/wble4jucHM.sWEBtMcaAh.Hklo+m1');
define('SECURE_AUTH_SALT', '4IK3vm|De-2/%.ql^x+4$7j>iwOX2KMK*s8_+6 gI_+zDJKP6?0yc*;PZ`Aw`mpq');
define('LOGGED_IN_SALT',   '`+cUovK<w{!_R8;F=@[t`GDM2-/6|GD@5sQ;NMaFMfOo4|GJ$X-W#Yf:m:mI4zlU');
define('NONCE_SALT',       ',=$/`X}dMv^*6ZYzw+I7%M}yF_ek<io6R.bIyMrNKKoq<-y?N7aB*QVrb]uA-F7:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
